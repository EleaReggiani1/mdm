class SparseArray:

    def __init__(self, strings, queries):
        self.strings = strings
        self.queries = queries
        self.results = []

# Count occurences of a query in strings
    def countInStrings(self, query):
        result = 0
        for string in self.strings: 
            if string == query:
                result += 1
        return result 
        
# Core fonction that get results for each query
    def compute(self):
        self.results = []
        for query in self.queries:
            result = self.countInStrings(query)
            self.results.append(result)

# Assign results to queries in a dictionary
    def getDictionary(self):
        if len(self.results) == len(self.queries):
            dictionary = dict(zip(self.queries, self.results))
            return dictionary