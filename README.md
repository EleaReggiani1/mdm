# README

## Description

This program is a simple flask server running in a docker container.
It allows to compare 2 lists of strings and return the result as a JSON dictionary.

## Prerequisites

- Docker

## Usage

First, build the container : 
``` bash
docker build . -t flask-elea
```
Then, run the container :
```bash
docker run -p 5000:5000 flask-elea
```

## How it works

Compare queries defined in the URL and strings defined in the Dockerfile.

## Example

```bash 
# In the dockerfile (strings)
...
ENV STRINGS abc,bbc,b,aa,a,aa,a,b,c
...

# In the URL (queries)
http://127.0.0.1:5000/mdm?queries=abc,bb,aa

# Response 
{"aa":2,"abc":1,"bb":0}
```