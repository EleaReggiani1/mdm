from SparseArray import SparseArray
import os 
from flask import Flask
from flask import request

app = Flask(__name__)

@app.route("/mdm")
def myRoute():
    arg = request.args.get('queries', default = '', type = str)
    queries = arg.split(sep=',')
    strings = varenv()
    sparse = SparseArray(strings, queries)
    sparse.compute()
    dictionary = sparse.getDictionary()
    return dictionary

def help():
    print('Please check the README.md')
    exit()

# Get the environement variable STRINGS
def varenv():
    env = os.environ
    value = env.get('STRINGS')
    if value is not None:
        return value.split(sep=',')
    else:
        help() 

if __name__ == "__main__":
    app.run(host='0.0.0.0')

