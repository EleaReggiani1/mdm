FROM python:3.7

RUN pip install --user flask

ENV STRINGS abc,bbc,b,aa,a,aa,a,b,c
ENV PATH="/root/.local/bin:${PATH}"

COPY main.py ./main.py
COPY SparseArray.py  ./SparseArray.py
COPY README.md ./README.md

ENTRYPOINT ["python3.7","./main.py"]
